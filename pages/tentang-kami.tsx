import { Box, Button, Flex, Image, Text } from "@chakra-ui/react";
import Header from "@/components/global/header";
import Background from "@/components/global/background";
import Head from "next/head";

export default function tentangKami() {
  return (
    <div>
      <Head>
        <title>About - PUPUK</title>
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <Flex
        w="100%"
        minH="100vh"
        bgColor="#ECFFF4"
        direction="column"
        position="relative"
      >
        <Box display={{ base: "none", md: "block" }}>
          <Background />
        </Box>
        <Header title="Tentang Kami" />
        <Flex mt="70px" mb="30px" px={{ base: "30px", md: "50px" }}>
          <Image
            mr="50px"
            borderRadius="100px 0px"
            src="../images/artikel/img-tentangkami2.png"
            alt="Tentang Kami"
            display={{ base: "none", md: "none", lg: "flex", xl: "flex" }}
            w={{ lg: "450px", xl: "576px" }}
            h={{ lg: "450px", xl: "580px" }}
            objectFit="cover"
            zIndex="1"
          />
          <Text
            fontSize={{ base: "15px", md: "20px", xl: "26px" }}
            color="black"
            textAlign="justify"
            zIndex="1"
          >
            Perhutanan Sosial adalah sistem pengelolaan hutan lestari yang
            dilaksanakan dalam kawasan hutan negara atau Hutan Hak/Hutan Adat
            yang dilaksanakan oleh Masyarakat setempat atau Masyarakat Hukum
            Adat sebagai pelaku utama untuk meningkatkan kesejahteraannya,
            keseimbangan lingkungan dan dinamika sosial budaya dalam bentuk
            Hutan Desa, Hutan Kemasyarakatan, Hutan Tanaman Rakyat, hutan adat
            dan kemitraan kehutanan.
            <br /> <br />
            Sebagai tindak lanjut pasca persetujuan izin perhutanan sosial,
            pemerintah mendorong Kelompok Perhutanan Sosial (KPS) membentuk
            Kelompok Usaha Perhutanan Sosial (KUPS) yang nantinya berfungsi
            sebagai unit bisnis. Berdasarkan data per 13 Maret 2023 telah
            terbentuk 8.041 Kelompok Usaha Perhutanan Sosial (KUPS), yang dibagi
            kedalam 4 kategori tingkatan: blue 4.793 (47,29%), silver 4.353
            (42%), gold 939 (9,26%) dan platinum 50 (0,49%).
          </Text>
        </Flex>
        <Flex px={{ base: "30px", md: "50px" }} mb="70px">
          <Text
            fontSize={{ base: "15px", md: "20px", xl: "26px" }}
            color="black"
            textAlign="justify"
            zIndex="1"
          >
            The Asia Foundation (TAF) bersama dengan Perkumpulan Untuk
            Peningkatan Usaha Kecil (PUPUK) telah menginisiasi berbagai macam
            program dengan memberi perhatian terhadap upaya-upaya perbaikan tata
            kelola hutan dan lahan di Indonesia. Program dijalankan secara
            kolaboratif dengan menggandeng OMS di tingkat lokal dan nasional,
            dimana PUPUK sebagai salah satu mitra di tingkat nasional dengan
            fokus kerja pada penguatan kapasitas OMS di daerah dalam
            pendampingan usaha KPS pasca persetujuan izin perhutanan sosial.
            <br /> <br />
            Hasil assessment awal juga menunjukkan beragam permasalahan yang
            dihadapi KPS dalam kegiatan usaha perhutanan sosial, mulai dari
            permasalahan sumber daya manusia hingga pasar. Umumnya permasalahan
            yang muncul merupakan gejala yang tampak akibat permasalahan lain
            yang lebih fundamental dalam ranatai nilai dan ekosistem pendukung
            usaha. Perbaikan ekosistem pendukung haruslah mengitegrasikan KPS
            dengan rantai nilai dan institusi pendukungnya secara berkelanjutan.
            Maka pendampingan penguatan usaha KPS haruslah menciptakan sebuah
            model bisnis dengan hubungan komersial yang saling menguntungkan
            diantara KPS dan para pelaku pasar lainnya. Pendampingan semacam ini
            menuntut ketrampilan pendamping baik dalam memahami konsep dan
            operasional tools yang digunakan dalam pengembangan usaha. Untuk itu
            TAF bersama dengan PUPUK kemudian menciptakan model pelatihan bagi
            pendamping mendamping usaha berbasis Perhutanan Sosial.Diharapkan
            dari pelatihan ini, pendamping usaha di daerah dampingannya nanti
            dapat memiliki keterampilan dalam mengembangkan kegiatan usaha
            kelompok perhutanan sosial.
          </Text>
        </Flex>
      </Flex>
    </div>
  );
}
