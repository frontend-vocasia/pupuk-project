import Contact from "@/components/tentang-kami/kontakItem";
import { Box, Button, Flex, Image, Text } from "@chakra-ui/react";
import Head from "next/head";
import Background from "@/components/global/background";
import Header from "@/components/global/header";

export default function Kontak() {
  return (
    <div>
      <Head>
        <title>Contact - PUPUK</title>
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <Flex       
        w="100%"
        minH="100vh"
        bgColor="#ECFFF4"
        direction="column"
        position="relative"
      >
        <Box display={{base: 'none', md: 'block'}}>
          <Background />
        </Box>
        <Header title='Kontak Kami'/>
        <Flex px={{ base: "25px", md: "70px" }}>
          <Contact />
        </Flex>
      </Flex>
    </div>
  );
}
