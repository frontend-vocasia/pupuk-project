import React from "react";
import Head from "next/head";
import ContainerKursus from "@/components/kursus/containerKursus";
import Background from "@/components/global/background";
import ContainerCardKursus from "@/components/kursus/containerCardKursus";
import KursusCard from "@/components/kursus/kursusCard";
import ComingsoonCard from "@/components/kursus/comingsoonCard";
import Header from "@/components/global/header";
import cover from "@/public/images/kursus/course_thumbnail_default_473.jpg"
import subCard from "@/public/icons/Hot Icon.svg"

type Props = {};

export default function Kursus({}: Props) {
  return (
    <>
      <Head>
        <title>Kursus - PUPUK</title>
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <ContainerKursus>
        <Background />
        <Header title={"Kursus"} />
        <ContainerCardKursus>
          <KursusCard
            to="https://vocasia.id/home/course/pengembangan-usaha-komoditas-perhutanan-sosial-bagi-kups-yang-responsif-gender/473"
            title="Pengembangan Usaha Berbasis Komoditas Perhutanan Sosial Yang Responsif Gender"
            description={[
              <ul style={{ listStyleType: "disc" }} key="0">
                <li>
                  Seleksi Komoditas Perhutanan Sosial Yang Responsif Gender
                </li>
                <li>
                  Menemukan Ide Bisnis Komoditas Perhutanan Sosial Yang
                  Responsif Gender
                </li>
                <li>Rencana Usaha Perhutanan Sosial</li>
                <li>Pemasaran Produk</li>
                <li>Kemasan Produk</li>
              </ul>,
            ]}
            imgCover={cover}
            iconTitle={subCard}
          />
          <ComingsoonCard />
          <ComingsoonCard />
          <ComingsoonCard />
          <ComingsoonCard />
          <ComingsoonCard />
        </ContainerCardKursus>
      </ContainerKursus>
    </>
  );
}
