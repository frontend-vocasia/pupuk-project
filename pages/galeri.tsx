import Gallery from "@/components/gallery";
import Head from "next/head";

type Props = {};

function Galleri(): JSX.Element {
  return (
    <>
      <Head>
        <title>Galeri - PUPUK</title>
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <Gallery />
    </>
  );
}

export default Galleri;
