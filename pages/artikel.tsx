import { Box, Flex, Image, Text } from "@chakra-ui/react";
import CardArtikel from "@/components/artikel/artikelCard";
import ContainerCard from "@/components/artikel/containerCard";
import Head from "next/head";
import Background from "@/components/global/background";
import Header from "@/components/global/header";

export default function artikel() {
  return (
    <>
      <Head>
        <title>Artikel - PUPUK</title>
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <Flex
        w="100%"
        minH="100vh"
        bgColor="#ECFFF4"
        direction="column"
        position="relative"
      >
        <Box display={{ base: "none", md: "block" }}>
          <Background />
        </Box>
        <Header title="Artikel" />
        <ContainerCard>
          <CardArtikel
            category="TAF & PUPUK"
            time="04 March 2023"
            imageSrc="../images/artikel/img-artikel1.jpg"
            title="Dukung Petani Milenial, Kemendes PDTT Terapkan Strategi 3 In 1"
            description="Pemerintah Indonesia telah berupaya memfasilitasi penyelenggaraan petani millenial khususnya di Papua."
            link="https://vocasia.id/blog/dukung-petani-milenial-kemendes-pdtt-terapkan-strategi-3-in-1/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="04 March 2023"
            imageSrc="../images/artikel/img-artikel2.jpg"
            title="Generasi Milenial Menjadi Fokus Utama Pembangunan Pertanian Indonesia"
            description="Kebijakan Presiden Joko Widodo untuk membangun sektor pertanian lebih banyak ditargetkan pada kawula muda."
            link="https://vocasia.id/blog/generasi-milenial-menjadi-fokus-utama-pembangunan-pertanian-indonesia/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="04 March 2023"
            imageSrc="../images/artikel/img-artikel3.png"
            title="Petani Milenial Berpotensi Jadi Tulang Punggung Pertanian Indonesia"
            description="Petani milenial memiliki potensi untuk jadi tulang punggung pertanian di Indonesia sebagaimana disampaikan Director of Environmental Governance TAF."
            link="https://vocasia.id/blog/petani-milenial-berpotensi-jadi-tulang-punggung-pertanian-indonesia/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="03 March 2023"
            imageSrc="../images/artikel/img-artikel4.jpg"
            title="Cara Unik Duta Petani Milenial Papua Barat Ajak Pemuda Berani Bertani"
            description="Duta Petani Milenial (DPM) Papua Barat, Malahayati mengatakan bahwa DPM mempunyai tugas untuk mengajak agar para pemuda tertarik untuk bertani."
            link="https://vocasia.id/blog/cara-unik-duta-petani-milenial-papua-barat-ajak-pemuda-berani-bertani/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="03 March 2023"
            imageSrc="../images/artikel/img-artikel5.jpg"
            title="Sektor Pertanian Berperan Penting Dalam Pertumbuhan Ekonomi"
            description="Sektor pertanian tumbuh positif di triwulan pertama tahun 2021. Namun sektor pertanian masih didominasi kalangan usia 40 tahun ke atas."
            link="https://vocasia.id/blog/sektor-pertanian-berperan-penting-dalam-pertumbuhan-ekonomi/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="03 March 2023"
            imageSrc="../images/artikel/img-artikel6.jpg"
            title="Program PAPeDA: Ciptakan Ketahanan Pangan Di Papua Melalui Budidaya Keladi"
            description="Festival Torang Pu Para Para yang digagas oleh Program Pertanian Berkelanjutan di Tanah Papua (PAPeDA) berupaya mengembangkan tanaman keladi."
            link="https://vocasia.id/blog/program-papeda-ciptakan/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="02 March 2023"
            imageSrc="../images/artikel/img-artikel7.jpg"
            title="Kemenkop UKM RI Minta Masyarakat Berkolaborasi Dengan Swasta Untuk Kemajuan UMKM"
            description="Deputi Kemenkop UKM RI, Siti Azizah menekankan pentingnya kolaborasi antara kelompok masyarakat."
            link="https://vocasia.id/blog/kemenkop-ukm-ri-minta-masyarakat-berkolaborasi-dengan-swasta-untuk-kemajuan-umkm/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="02 March 2023"
            imageSrc="../images/artikel/img-artikel8.jpg"
            title="4 Strategi Program PAPeDA Dalam Pengembangan Produk Lokal Papua"
            description=" Program Pelestarian Sumber Daya Alam dan Peningkatan Kehidupan Masyarakat Adat membeberkan empat (4) strateginya dalam mengembangkan produk lokal Papua."
            link="https://vocasia.id/blog/4-strategi-program-papeda-dalam-pengembangan-produk-lokal-papua/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="02 March 2023"
            imageSrc="../images/artikel/img-artikel9.jpg"
            title="Melalui Program SETAPAK, The Asia Foundation Dampingi Kelompok Perempuan Dan Masyarakat Untuk Akses Pengelolaan Perhutsos"
            description="TAF didukung oleh Forest Governance, Market, and Climate (FGMC) melaksanakan program Selamatkan Hutan dan Lahan Melalui Tata Kelola (SETAPAK) III"
            link="https://vocasia.id/blog/setapak-the-asia-foundation/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="02 March 2023"
            imageSrc="../images/artikel/img-artikel10.jpg"
            title="Kolaborasi TAF Dan PUPUK Surabaya Selenggarakan ToT Perhutanan Sosial Yang Responsif Gender"
            description="TAF PUPUK Surabaya mengadakan pelatihan Perhutanan Sosial untuk Perempuan dan Generasi Muda."
            link="https://vocasia.id/blog/kolaborasi-taf-dan-pupuk-surabaya-selenggarakan-tot-perhutanan-sosial-yang-responsif-gender-2/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="02 March 2023"
            imageSrc="../images/artikel/img-artikel11.png"
            title="Perempuan Tangguh “Penjaga Hutan” Berbagi Pengalaman Di 2 Universitas Ternama Surabaya"
            description="Perkenalkan, dialah Donsri, perempuan tangguh penjaga hutan asal Bengkulu."
            link="https://vocasia.id/blog/perempuan-tangguh-penjaga-hutan-berbagi-pengalaman-di-2-universitas-ternama-surabaya/"
          />
          <CardArtikel
            category="TAF & PUPUK"
            time="01 March 2023"
            imageSrc="../images/artikel/img-artikel12.jpg"
            title="Nowela Ajak Masyarakat Promosikan Produk Papua"
            description="Penyanyi Indonesia Idol, Nowela Elizabeth Mikhelia Auparay mengajak bersama-sama masyarakat untuk promosikan produk-produk asli dari Papua."
            link="https://vocasia.id/blog/kolaborasi-taf-dan-pupuk-surabaya-selenggarakan-tot-perhutanan-sosial-yang-responsif-gender-2/"
          />
        </ContainerCard>
      </Flex>
    </>
  );
}
