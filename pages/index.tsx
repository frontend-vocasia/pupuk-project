import Head from "next/head";
import Image from "next/image";
import styles from "@/styles/Home.module.css";
import { Flex, Text } from "@chakra-ui/react";
import LandingPage from "@/components/home";

export default function Home() {
  return (
    <>
      <Head>
        <title>PUPUK</title>
        <meta name="description" content="PUPUK - Perkumpulan untuk peningkatan Usaha Kecil" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="./images/Logo Header.png" />
      </Head>
      <main>
        <LandingPage />
      </main>
    </>
  );
}
