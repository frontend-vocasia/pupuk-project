import React, { ReactNode } from "react";
import NavbarLayout from "./navbarLayout";
import Footer from "./footer";

type Props = {
  children: ReactNode;
};

export default function Layout({ children }: Props) {
  return (
    <>
      <NavbarLayout />
      <main style={{ backgroundColor: "rgba(236, 255, 244, 1)" }}>
        {children}
      </main>
      <Footer />
    </>
  );
}
