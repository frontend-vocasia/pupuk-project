import { Flex, Text } from "@chakra-ui/react";

type Props = {};

export default function Footer({}: Props) {
  return (
    <footer>
      <Flex h="57px" bg="#01352C" color="white" justify="center" align="center">
        <Text>&#169; 2023 Vocasia. All Rights Reserved </Text>
      </Flex>
    </footer>
  );
}
