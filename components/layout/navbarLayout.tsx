import {
  Box,
  Flex,
  Text,
  IconButton,
  Stack,
  Collapse,
  Popover,
  PopoverTrigger,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import logo from "@/public/images/Logo PUPUK.png";
import logo2 from "@/public/images/Logo TAF.png";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import Home from "@/styles/Home.module.css";

export default function NavbarLayout() {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box>
      <Flex
        bg={useColorModeValue("rgba(233, 248, 239, 1)", "gray.800")}
        color={useColorModeValue("gray.600", "white")}
        minH={"70px"}
        py={{ base: 2 }}
        px={{ base: 4, md: 10 }}
        borderBottom={1}
        borderStyle={"solid"}
        borderColor={useColorModeValue("gray.200", "gray.900")}
        align={"center"}
      >
        <Flex flex={{ base: 1 }} justify="start">
          <Link href="/">
            <Flex>
              <Image
                src={logo}
                alt="PUPUK"
                style={{ width: "130px", height: "100%" }}
              />
              <Image
                src={logo2}
                alt="The Asia Foundation"
                style={{ width: "100px", height: "100%", marginLeft: "15px" }}
              />
            </Flex>
          </Link>
        </Flex>

        <Stack justify={"flex-end"} direction={"row"} spacing={6}>
          <Flex display={{ base: "none", md: "flex" }}>
            <DesktopNav />
          </Flex>
          <Flex
            flex={{ base: 1, md: "auto" }}
            ml={{ base: -2 }}
            display={{ base: "flex", md: "none" }}
          >
            <IconButton
              onClick={onToggle}
              icon={
                isOpen ? (
                  <CloseIcon w={3} h={3} />
                ) : (
                  <HamburgerIcon w={5} h={5} />
                )
              }
              variant={"ghost"}
              aria-label={"Toggle Navigation"}
            />
          </Flex>
        </Stack>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
}

const DesktopNav = () => {
  const linkHoverColor = useColorModeValue("rgba(1, 53, 44, 1)", "white");
  const router = useRouter();
  const currentRoute = router.pathname;

  return (
    <Stack direction={"row"} alignItems="center" spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Link
                href={navItem.href}
                className={
                  currentRoute === navItem.href ? Home.active : Home.nonActive
                }
              >
                <Text
                  p={2}
                  fontSize={"lg"}
                  fontWeight={600}
                  _hover={{
                    textDecoration: "none",
                    color: linkHoverColor,
                    fontWeight: "900",
                  }}
                >
                  {navItem.label}
                </Text>
              </Link>
            </PopoverTrigger>
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};

const MobileNav = () => {
  return (
    <Stack
      bg={useColorModeValue("rgba(233, 248, 239, 1)", "gray.800")}
      py={4}
      px={10}
      align="center"
      display={{ md: "none" }}
    >
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
};

const MobileNavItem = ({ label, children, href }: NavItem) => {
  const { isOpen, onToggle } = useDisclosure();
  const router = useRouter();
  const currentRoute = router.pathname;

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        px={5}
        as={Link}
        href={href}
        justify={"space-between"}
        borderRadius="12px"
        align={"center"}
        _hover={{
          textDecoration: "none",
        }}
        className={
          currentRoute === href ? Home.activeMobile : Home.nonActiveMobile
        }
      >
        <Text fontWeight={600}>{label}</Text>
      </Flex>

      <Collapse in={isOpen} animateOpacity style={{ marginTop: "0!important" }}>
        <Stack
          mt={2}
          pl={4}
          borderLeft={1}
          borderStyle={"solid"}
          borderColor={useColorModeValue("gray.200", "gray.700")}
          align={"start"}
        >
          {children &&
            children.map((child) => (
              <Text py={2} key={child.label}>
                <Link href={child.href}>{child.label}</Link>
              </Text>
            ))}
        </Stack>
      </Collapse>
    </Stack>
  );
};

interface NavItem {
  label: string;
  subLabel?: string;
  children?: Array<NavItem>;
  href?: any;
}

const NAV_ITEMS: Array<NavItem> = [
  {
    label: "Beranda",
    href: "/",
  },
  {
    label: "Kursus",
    href: "/kursus",
  },
  {
    label: "Artikel",
    href: "/artikel",
  },
  {
    label: "Galeri",
    href: "/galeri",
  },
  {
    label: "Tentang Kami",
    href: "/tentang-kami",
  },
  {
    label: "Kontak",
    href: "/kontak",
  },
];
