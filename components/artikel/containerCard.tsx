import { Stack, Wrap } from "@chakra-ui/react";

export default function ContainerCard({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Stack w="full" h="full" py={[4, 12]} px={[4, 5]} zIndex={10} mb='212px' mt= '70px'>
      <Wrap justify="center" spacing="65px" paddingBottom='10px'>
        {children}
      </Wrap>
    </Stack>
  );
}
