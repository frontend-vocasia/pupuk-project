import { Box, Button, Flex, Image, Link, Text, VStack } from "@chakra-ui/react";
import { ArrowForwardIcon } from "@chakra-ui/icons";

export default function CardArtikel({
  category,
  time,
  imageSrc,
  title,
  description,
  link
}: {
  category: string;
  time: string;
  imageSrc: string;
  title: string;
  description: string;
  link: string;
}) {
  
    return (
      <Flex
      id="card"
      direction="column"
      maxW="390px"
      bgColor="#FFF"
      overflow="hidden"
      borderRadius="16px"
      shadow='4px 10px 4px rgba(0, 0, 0, 0.2)'
    >
      <Flex id="cardImage">
        <Image src={imageSrc} alt="img" height={{ base: 200, md: 250 }} width="100%" objectFit="cover" />
      </Flex>
      <Flex id="cardContent" direction="column" p="25px" flex={1}>
        <Flex mb='10px' justify="space-between">
          <Text fontSize="12px" fontWeight="semibold" color="#6E798C">
            {category}
          </Text>
          <Text fontSize="12px" fontWeight="semibold" color="#6E798C">
            {time}
          </Text>
        </Flex>

        <Text fontSize="18px" fontWeight="bold" mb="20px" color="black">
          {title}
        </Text>
        <Text fontSize="13px" fontWeight="normal"  color="black">
          {description}
        </Text>
      </Flex>
      <Flex
        id="cardAction"
        mb="25px"
        align="center"
        justifyContent='center'
      >
        <Link href={link} >
          <Button
            variant='outline'
            w={{ base: "133px", md: "150px" }}
            h={{ base: "33px", md: "40px" }}
            fontSize='12px'
            color='#2ECC71'
            borderColor="#2ECC71"
            borderRadius='33px'
            _hover={{ bgColor: "#01352C", color: "white" }}
          >
            Baca Selengkapnya
          </Button>
        </Link>
      </Flex>
    </Flex>
  );
};
