import { Stack, Wrap } from "@chakra-ui/react";

export default function ContainerCardKursus({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Stack
      w="full"
      h="full"
      py={[4, 8, 12]}
      px={4}
      // px="auto"
      zIndex={10}
    >
      <Wrap justify="center" spacing={["8px", "16px", "28px", "36px"]}>
        {children}
      </Wrap>
    </Stack>
  );
}
