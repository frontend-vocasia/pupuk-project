import { Flex } from "@chakra-ui/react";

export default function Category({ children }: { children: React.ReactNode }) {
  return (
    <Flex
      w="full"
      h="auto"
      my={{ base: 4, md: 8, lg: 12 }}
      px={{ base: 0, md: "60px", lg: "68px" }}
      zIndex={10}
      justify="center"
      flexWrap={["wrap", "unset"]}
    >
      {children}
    </Flex>
  );
}
