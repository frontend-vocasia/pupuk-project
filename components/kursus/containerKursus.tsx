import { Flex } from "@chakra-ui/react";

export default function ContainerKursus({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Flex
      w="100%"
      minH="100vh"
      bgColor="#ECFFF4"
      direction="column"
      position="relative"
    >
      {children}
    </Flex>
  );
}
