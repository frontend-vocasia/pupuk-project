import React from "react";
import Image from "next/image";
import { Flex, Text } from "@chakra-ui/react";
import imgComingsoon from "public/images/comingsoon.svg";

type Props = {};

export default function ComingsoonCard({}: Props) {
  return (
    <Flex
      id="card"
      maxW={{ base: "300px", lg: "280px", xl: "360px" }}
      minH="528px"
      overflow="hidden"
      borderRadius="16px"
      direction="column"
      bgColor="rgba(0, 0, 0, 0.2)"
      boxShadow="md"
      _hover={{ boxShadow: "xl" }}
      filter="drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))"
    >
      <Flex
        id="cardContent"
        p={4}
        direction="column"
        flex={1}
        align="center"
        justify="center"
      >
        <Image src={imgComingsoon} alt="image-bakground" />
        <Text
          mt={4}
          fontSize={"24px"}
          fontWeight="bold"
          textAlign="center"
          color="#C00B12"
        >
          COMING SOON
        </Text>
      </Flex>
    </Flex>
  );
}
