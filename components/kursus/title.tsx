import { Heading } from "@chakra-ui/react";
import React from "react";

type Props = {
  title: string;
};

export default function Title({ title }: Props) {
  return (
    <Heading
      ml={{ base: "16px", md: "64px", lg: "70px" }}
      fontSize={["48px", "60px"]}
      fontWeight="bold"
      zIndex={10}
      textAlign={["center", "left"]}
    >
      {title}
    </Heading>
  );
}
