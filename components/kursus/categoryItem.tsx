import Image from "next/image";
import iconArrow from "public/icons/icon-arrow.svg";
import { Flex, Link, Text } from "@chakra-ui/react";

export default function CategoryItem({
  to,
  title,
  icon,
}: {
  to: string;
  title: string;
  icon: string;
}) {
  return (
    <Link href={to} w={["42%", "25%"]} my={[1, 0]} mx={["2%", 2]} h="auto">
      <Flex
        w="full"
        h="full"
        py={{ base: 1, md: 2, lg: 2 }}
        px={{ base: 2, md: 4, lg: 4 }}
        borderRadius={8}
        bgColor="#BAD8B6"
        align="center"
        justify="space-between"
        zIndex={10}
      >
        <Flex>
          <Flex w={{ base: 0, md: 4, lg: 6 }}>
            <Image src={icon} alt="icon kursus" />
          </Flex>
          <Text
            fontSize={{ base: "16px", md: "16px", lg: "24px" }}
            fontWeight={"medium"}
            margin={2}
          >
            {title}
          </Text>
        </Flex>
        <Flex w={{ base: 4, md: 4, lg: 6 }}>
          <Image src={iconArrow} alt="icon arrow kursus" />
        </Flex>
      </Flex>
    </Link>
  );
}
