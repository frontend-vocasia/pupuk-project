import Image from "next/image";
import infoIcon from "public/icons/Info Icon.svg";
import { Flex, Text, Button, Link } from "@chakra-ui/react";

export default function KursusCard({
  to,
  title,
  description,
  imgCover,
  iconTitle,
}: {
  to: string;
  title: string;
  description: any;
  imgCover: any;
  iconTitle: any;
}) {
  return (
    <Flex
      id="card"
      maxW={{ base: "300px", lg: "280px", xl: "360px" }}
      h="auto"
      overflow="hidden"
      borderRadius="16px"
      direction="column"
      bgColor="#FFF"
      boxShadow="md"
      _hover={{ boxShadow: "xl" }}
      filter="drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))"
    >
      <Flex id="cardImgCover" position="relative" direction="column">
        <Image
          src={imgCover}
          alt="image cover kursus"
          width={500}
          height={500}
          priority
        />
        <Flex position="absolute" right={0} top={0} zIndex={20}>
          <Image src={infoIcon} alt={title} />
        </Flex>
      </Flex>
      <Flex id="cardContent" px={4} direction="column" flex={1} w="full">
        <Flex justify="space-between" align="flex-start" w="full" my={4}>
          <Flex w="90%">
            <Text fontSize="2xl" fontWeight="bold" noOfLines={2}>
              {title}
            </Text>
          </Flex>
          <Flex w="10%" mt={1}>
            {/* {iconTitle} */}
            <Image src={iconTitle} alt="icon" width={500} height={500} />
          </Flex>
        </Flex>

        <Flex fontSize="md" fontWeight="normal" px={4}>
          {description.map((e: any) => (
            <div key={e.key}>{e}</div>
          ))}
        </Flex>
      </Flex>
      <Flex id="cardAction" p={4} w="full">
        <Link w="full" href={to}>
          <Button
            w="full"
            bgColor="#2ECC71"
            borderRadius={8}
            color="white"
            _hover={{ bgColor: "#01352C" }}
          >
            Miliki Sekarang
          </Button>
        </Link>
      </Flex>
    </Flex>
  );
}
