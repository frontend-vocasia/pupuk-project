import React from "react";
import Main from "./main";
import AboutUs from "./aboutus";
import Kursus from "./kursus";
import Artikel from "./artikel";
import Testi from "./testi";
import { Box } from "@chakra-ui/react";

type Props = {};

export default function LandingPage({}: Props) {
  return (
    <div>
      <Box>
        <Main />
        <AboutUs />
        <Kursus />
        <Artikel />
        <Testi />
      </Box>
    </div>
  );
}
