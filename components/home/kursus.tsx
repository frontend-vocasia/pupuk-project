import React from "react";
import Image from "next/image";
import {
  Box,
  Card,
  CardBody,
  Text,
  Center,
  Grid,
  GridItem,
  Flex,
} from "@chakra-ui/react";
import KursusCard from "./kursusCard";
import ImgBgGaris from "../../public/images/img-bg-garis.svg";
import ImgRafiki from "../../public/images/img-rafiki.svg";
import imgComingsoon from "../../public/images/comingsoon.svg";

type Props = {};

export default function Kursus({}: Props) {
  return (
    <>
      <Box
        mt={90}
        mx={{ xl: "70px", base: "20px" }}
        zIndex={4}
        position="relative"
      >
        <Center>
          <Text fontSize={"45px"} color="#01352C" fontWeight={700}>
            Kursus
          </Text>
        </Center>

        <Box mt={{ base: "10px", xl: "34px" }}>
          <Grid
            templateColumns={{
              xl: "repeat(4, 1fr)",
              lg: "repeat(2, 1fr)",
              md: "repeat(2, 1fr)",
              sm: "repeat(1, 1fr)",
            }}
            gap="48px"
          >
            <KursusCard
              title="Pengembangan Usaha Berbasis Komoditas...."
              description={[
                <ol key="">
                  <li>
                    Seleksi Komoditas Perhutanan Sosial Yang Responsif Gender
                  </li>
                  <li>
                    Menemukan Ide Bisnis Komoditas Perhutanan Sosial Yang
                    Responsif Gender
                  </li>
                  <li>Membuat Business Model Canvas Bagi KUPS.....</li>
                </ol>,
              ]}
              to="https://vocasia.id/home/course/pengembangan-usaha-komoditas-perhutanan-sosial-bagi-kups-yang-responsif-gender/473"
            />
            <Flex
              id="card"
              // maxW={{ base: "300px", lg: "280px", xl: "360px" }}
              minH="528px"
              overflow="hidden"
              borderRadius="16px"
              direction="column"
              bgColor="#C4C4C4"
              boxShadow="md"
              _hover={{ boxShadow: "xl" }}
              filter= 'drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))'
            >
              <Flex
                id="cardContent"
                p={4}
                direction="column"
                flex={1}
                align="center"
                justify="center"
              >
                <Image src={imgComingsoon} alt="image-bakground" />
                <Text
                  mt={4}
                  fontSize={"24px"}
                  fontWeight="bold"
                  textAlign="center"
                  color="#C00B12"
                >
                  COMING SOON
                </Text>
              </Flex>
            </Flex>
            <Flex
              id="card"
              // maxW={{ base: "300px", lg: "280px", xl: "360px" }}
              minH="528px"
              overflow="hidden"
              borderRadius="16px"
              direction="column"
              bgColor="#C4C4C4"
              boxShadow="md"
              _hover={{ boxShadow: "xl" }}
              filter= 'drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))'
            >
              <Flex
                id="cardContent"
                p={4}
                direction="column"
                flex={1}
                align="center"
                justify="center"
              >
                <Image src={imgComingsoon} alt="image-bakground" />
                <Text
                  mt={4}
                  fontSize={"24px"}
                  fontWeight="bold"
                  textAlign="center"
                  color="#C00B12"
                >
                  COMING SOON
                </Text>
              </Flex>
            </Flex>
            <Flex
              id="card"
              // maxW={{ base: "300px", lg: "280px", xl: "360px" }}
              minH="528px"
              overflow="hidden"
              borderRadius="16px"
              direction="column"
              bgColor="#C4C4C4"
              boxShadow="md"
              _hover={{ boxShadow: "xl" }}
              filter= 'drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))'
            >
              <Flex
                id="cardContent"
                p={4}
                direction="column"
                flex={1}
                align="center"
                justify="center"
              >
                <Image src={imgComingsoon} alt="image-bakground" />
                <Text
                  mt={4}
                  fontSize={"24px"}
                  fontWeight="bold"
                  textAlign="center"
                  color="#C00B12"
                >
                  COMING SOON
                </Text>
              </Flex>
            </Flex>
          </Grid>
        </Box>
      </Box>
      <Box position="absolute" zIndex={0} mt="-80px">
        <Image src={ImgBgGaris} alt={ImgBgGaris} />
      </Box>
    </>
  );
}
