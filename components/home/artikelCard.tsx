import {
  Box,
  Card,
  CardBody,
  Flex,
  Text,
  Stack,
  Heading,
  chakra,
} from "@chakra-ui/react";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import { AiOutlineClockCircle } from 'react-icons/ai'

type Props = {};

export default function ArtikelCard({
  img,
  title,
  desc,
  date,
  link
}: {
  img: any;
  title: string;
  desc: string;
  date: string;
  link: string;
}) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) =>
      ["width", "height", "src", "alt"].includes(prop),
  });

  return (
    <div>
      <Card bgColor="#59AA7A" borderRadius={{ base: "20px", xl: "12px" }}>
        <Flex flexDir={{ base: "column", xl: "row", lg: "row", md: "row" }}>
          <OurImage
            src={img}
            alt={img}
            width={{
              base: "100%",
              xl: "272px",
              lg: "272px",
              sm: "100%",
              md: "272px",
            }}
            height={{ base: "100%", xl: "197px" }}
          />
          <Stack>
            <CardBody>
              <Heading
                fontSize="14px"
                fontWeight={700}
                bgColor="#BAD8B6"
                display="inline-block"
                p="3px 21px"
                borderRadius="10px"
              >
                {title}
              </Heading>
              <Link href={link}>
                <Text
                  pt="2"
                  pb="20px"
                  fontSize="15px"
                  fontWeight={400}
                  color="white"
                  mt='16px'
                >
                  {desc}
                </Text>
              </Link>
              <Box
                position="absolute"
                bottom="15px"
                // right="20px"
                color="rgba(0, 0, 0, 0.7)"
                fontSize="14px"
                display='flex'
                alignItems='center'
              >
                <AiOutlineClockCircle style={{ marginRight: '8px' }}/>
                {date}
              </Box>
            </CardBody>
          </Stack>
        </Flex>
      </Card>
    </div>
  );
}
