import React, { useState } from "react";
import TestiCard from "./testiCard";
import { Box, Center, Text, useBreakpointValue } from "@chakra-ui/react";
import Slider from "react-slick";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";
import LandingPage from "../../styles/LandingPage.module.css";
import ImgBgCircle from "../../public/images/img-bg-circle.svg";
import Image from "next/image";
import ImageTestiRev from "../../public/images/img-testi-rev.svg";
import ImageTestiRev2 from "../../public/images/img-testi-rev-2.svg";
import ImageTestiRev3 from "../../public/images/img-testi-rev-3.svg";
import LogoTAF from "../../public/images/Logo-TAF.svg";
import SwiperCore, { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import ImageMasri from "../../public/Masri.svg";
import ImageWulan from "../../public/Wulan.svg";
import ImageFatmawati from "../../public/Fatmawati.svg";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type Props = {};

export default function Testi({}: Props) {
  type ArrowProps = {
    onClick?: () => void;
    className?: string;
  };

  const PrevArrow = ({ onClick }: ArrowProps) => (
    <div
      className={`${LandingPage.slickArrow} ${LandingPage.slickPrev} slick-prev`}
      onClick={onClick}
    >
      <FaAngleLeft />
    </div>
  );

  const NextArrow = ({ onClick }: ArrowProps) => (
    <Box
      className={`${LandingPage.slickArrow} ${LandingPage.slickNext} slick-next`}
      onClick={onClick}
    >
      <FaAngleRight />
    </Box>
  );

  // const settings = {
  //   dots: true,
  //   infinite: false,
  //   speed: 500,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   prevArrow: <PrevArrow />,
  //   nextArrow: <NextArrow />,
  //   beforeChange: (current: number, next: number) => setCardIndex(next),
  // };

  // const isSmallScreen = useBreakpointValue({
  //   base: true,
  //   lg: false,
  //   xl: false,
  //   md: false,
  //   sm: true,
  //   xs: true,
  // });

  // const [cardIndex, setCardIndex] = useState(0);
  SwiperCore.use([Navigation, Pagination]);

  const cardItem: { id: number; image: any; text: string; city: string }[] = [
    {
      id: 1,
      image: ImageMasri,
      text: "“Dengan mengikuti pelatihan Pendamping KUPS responsif gender, saya lebih memahami cara menghitung biaya produksi dan mengetahui aktor pasar yang terlibat serta pendekatan atau pendampingan yang dilakukan kepada kelompok perempuan sedikit berbeda dengan KUPS pada umumnya. Hal ini dikarenakan pendamping harus memperhatikan waktu kerja perempuan disektor domestik yang masih dikerjakan oleh sebagaian besar kelompok perempuan.”",
      city: "Masri, Kalimantan Barat",
    },
    {
      id: 2,
      image: ImageWulan,
      text: "“Pasca Mengikuti Kelas Pelatihan untuk pendamping KUPS yang responsif Gender, saya lebih dikuatkan menjadi pendamping untuk kelompok perempuan dan ternyata benar pendamping harus lebih peka dan responsif. Hal paling utama yang harus dilakukan adalah menyesuaikan waktu Pertemuan dengan Komunitas setelah perempuan selesai melakukan Urusan rumah tangga, biar angka partisipasi perempuan lebih banyak dan mereka lebih tenang ketika sedang berkegiatan.”",
      city: "Wulan Lembonunu, Sulawesi Tengah",
    },
    {
      id: 3,
      image: ImageFatmawati,
      text: "“Hal yang saya dapatkan dari pelatihan ini adalah bahwa saya harus memiliki cara pandang tentang diri saya dan orang lain. Selain melihat peluang usaha, saya juga harus menjaga kepercayaan konsumen maupun stakholder. Dan saya tahu akan resiko yg di hadapi oleh pengusaha serta analisis bisnis dan riset untk meminimalisir kesalahan/kerugian”",
      city: "Fatmawati, Papua",
    },
  ];

  // const mapArr = cardItem.map((x) => x.image)
  // console.log(mapArr)

  return (
    <div>
      <Box
        mt={70}
        pb={79}
        mx={{ xl: "85px", base: "25px" }}
        position="relative"
        zIndex={3}
      >
        <Center>
          <Text fontSize={"45px"} color="#01352C" fontWeight={700}>
            Testimonial
          </Text>
        </Center>
        <Box>
          <Swiper
            slidesPerView={1}
            spaceBetween={0}
            centeredSlides={true}
            pagination={{ clickable: true }}
            navigation
          >
            {cardItem.map((item) => (
              <SwiperSlide key={item.id}>
                <TestiCard
                  image={item.image}
                  text={item.text}
                  name={item.city}
                />
              </SwiperSlide>
            ))}
          </Swiper>
          {/* <Slider {...settings}>
            {cardItem.map((item, index) => (
              <Box
                key={index}
              >
                <TestiCard
                  image={item.image}
                  name={item.city}
                  text={item.text}
                ></TestiCard>
              </Box>
            ))}
          </Slider> */}
        </Box>
      </Box>
      {/* <Box pb={{ xl: "80px", lg: "80px", base: "40px" }}>
        <Box>
          <Text
            fontSize="45px"
            fontWeight={700}
            textAlign="center"
            mb="30px"
            color="#01352C"
          >
            Didukung Oleh
          </Text>
          <Center>
            <Image src={LogoTAF} alt={LogoTAF} style={{ width: "220px" }} />
          </Center>
        </Box>
      </Box> */}
      <Box position="absolute" left="0" zIndex={0} mt="-360px">
        <Image
          src={ImgBgCircle}
          alt={ImgBgCircle}
          style={{ height: "400px" }}
        />
      </Box>
    </div>
  );
}
