import {
  Box,
  Flex,
  Card,
  CardBody,
  Text,
  Button,
  chakra,
  Center,
} from "@chakra-ui/react";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import ImgAbout from "../../public/images/img-about.svg";
import ImgAboutUs from "../../public/images/img-about-us.svg";
import ImgTentang from "../../public/img-tentang.svg";
import ImgBgJaring from "../../public/images/img-bg-jaring.svg";

type Props = {};

export default function AboutUs({}: Props) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) =>
      ["width", "height", "src", "alt"].includes(prop),
  });

  return (
    <>
      <Box>
        <Flex
          mt={{ base: "40px", xl: "90px" }}
          mx={{ base: "20px", xl: "70px" }}
          flexDirection={{ base: "column", xl: "row" }}
          zIndex={3}
          position="relative"
        >
          <Flex>
            <Box w={{ base: "100%", xl: "527px" }}>
              <OurImage
                src={ImgTentang}
                alt="Dan Abramov"
                width="100%"
                height="100%"
                borderRadius="15px"
              />
            </Box>
          </Flex>
          <Flex ml={{ base: "0", xl: "27px" }} mt={{ base: "20px", xl: "0" }}>
            <Box>
              <Card bg={"#59AA7A"} borderRadius="15px" h={"100%"}>
                <CardBody
                  display="flex"
                  flexDirection="column"
                  justifyContent="space-between"
                  p="33px 40px 20px 40px"
                >
                  <Box>
                    <Text
                      fontSize="28px"
                      color="white"
                      fontWeight={700}
                      textAlign={{ base: "center", xl: "left" }}
                    >
                      Tentang Kami
                    </Text>
                    <Text
                      fontSize={{ base: "15px", md: "17px", xl: "19px" }}
                      color="white"
                      textAlign={{ base: "justify", xl: "left" }}
                      mt={{ base: "8px", xl: "5px" }}
                    >
                      Perhutanan Sosial adalah sistem pengelolaan hutan lestari
                      yang dilaksanakan dalam kawasan hutan negara atau Hutan
                      Hak/Hutan Adat yang dilaksanakan oleh Masyarakat setempat
                      atau Masyarakat Hukum Adat sebagai pelaku utama untuk
                      meningkatkan kesejahteraannya, keseimbangan lingkungan dan
                      dinamika sosial budaya dalam bentuk Hutan Desa, Hutan
                      Kemasyarakatan, Hutan Tanaman Rakyat, hutan adat dan
                      kemitraan kehutanan.
                    </Text>
                  </Box>
                  <Center>
                    <Link href="/tentang-kami">
                      <Button
                        // variant="solid"
                        // bgColor="#E1EACD"
                        // fontSize={{ base: "18px", xl: "20px" }}
                        // fontWeight={700}
                        // marginTop={{ xl: "15px", base: "20px" }}
                        // width={{ base: "100%", xl: "35%" }}
                        variant="solid"
                        bgColor="transparent"
                        fontSize={{ base: "18px", xl: "20px" }}
                        fontWeight={400}
                        color="white"
                        _hover={{
                          bgColor: "white",
                          color: "#59AA7A",
                        }}
                        borderRadius="33px"
                        border="2px solid #fff"
                        p="19px 31px"
                      >
                        Selengkapnya
                      </Button>
                    </Link>
                  </Center>
                </CardBody>
              </Card>
            </Box>
          </Flex>
        </Flex>
        <Box position="absolute" right="0" mt="-80px" zIndex={0}>
          <Image src={ImgBgJaring} alt={ImgBgJaring} />
        </Box>
      </Box>
    </>
  );
}
