import {
  Box,
  Center,
  Grid,
  GridItem,
  Text,
  Link,
  Button,
} from "@chakra-ui/react";
import React from "react";
import ArtikelCard from "./artikelCard";
import CardArtikel from "../../public/images/img-artikel.svg";
import CardArtikel2 from "../../public/images/img-artikel-2.svg";
import CardArtikel3 from "../../public/images/img-artikel-3.svg";
import CardArtikel4 from "../../public/images/img-artikel-4.svg";
import CardArtikel5 from "../../public/images/img-artikel-5.svg";
import CardArtikel6 from "../../public/images/img-artikel-6.svg";
import NextLink from "next/link";

type Props = {};

export default function Artikel({}: Props) {
  return (
    <div>
      <Box
        mt={90}
        mx={{ base: "20px", xl: "70px" }}
        position="relative"
        zIndex={3}
      >
        <Center>
          <Text fontSize={"45px"} color="#01352C" fontWeight={700} mb="10px">
            Artikel
          </Text>
        </Center>

        <Box mt={{ xl: "34px", base: "0" }}>
          <Grid
            templateColumns={{ xl: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
            gap={5}
          >
            <GridItem>
              <ArtikelCard
                img={CardArtikel}
                title="Nasional"
                desc="Melalui Program SETAPAK, The Asia Foundation Dampingi Kelompok Perempuan dan Masyarakat untuk Akses Pengelolaan Perhutsos"
                date="24/11/2022"
                link="https://vocasia.id/blog/setapak-the-asia-foundation/"
              />
            </GridItem>
            <GridItem>
              <ArtikelCard
                img={CardArtikel3}
                title="Nasional"
                desc="Tingkatkan Produktivitas, Dinas TPHP Papua Barat Fokus Alokasikan Anggaran untuk Pengembangan Keladi"
                date="18/08/2021"
                link="https://vocasia.id/blog/tingkatkan-produktivitas-dinas-tphp-papua-barat-fokus-alokasikan-anggaran-untuk-pengembangan-keladi/"
              />
            </GridItem>
          </Grid>
        </Box>
        <Box mt={23}>
          <Grid
            templateColumns={{ xl: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
            gap={5}
          >
            <GridItem>
              <ArtikelCard
                img={CardArtikel2}
                title="Nasional"
                desc="4 Strategi Program PAPeDA dalam Pengembangan Produk Lokal Papua
                "
                date="27/08/2021"
                link="https://vocasia.id/blog/4-strategi-program-papeda-dalam-pengembangan-produk-lokal-papua/"
              />
            </GridItem>
            <GridItem>
              <ArtikelCard
                img={CardArtikel4}
                title="Nasional"
                desc="Kolaborasi TAF dan PUPUK Surabaya Selenggarakan ToT Perhutanan Sosial Yang Responsif Gender"
                date="06/08/2022"
                link="https://vocasia.id/blog/kolaborasi-taf-dan-pupuk-surabaya-selenggarakan-tot-perhutanan-sosial-yang-responsif-gender-2/"
              />
            </GridItem>
          </Grid>
        </Box>
        <Box mt={23}>
          <Grid
            templateColumns={{ xl: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
            gap={5}
          >
            <GridItem>
              <ArtikelCard
                img={CardArtikel5}
                title="Nasional"
                desc="Nowela Ajak Masyarakat Promosikan Produk Papua"
                date="03/02/2023"
                link="https://vocasia.id/blog/nowela-ajak-masyarakat-promosikan-produk-papua/"
              />
            </GridItem>
            <GridItem>
              <ArtikelCard
                img={CardArtikel6}
                title="Nasional"
                desc="Krisis Iklim Ancam Dunia, APSSI Gelar Workshop Green Digital Society"
                date="11/11/2022"
                link="https://vocasia.id/blog/category/taf-pupuk/"
              />
            </GridItem>
          </Grid>
          <Center>
            <Box mt="50px">
              <Link href="/artikel" style={{ textDecoration: "none" }}>
                <Button
                  variant="solid"
                  bgColor="transparent"
                  fontSize={{ base: "18px", xl: "20px" }}
                  fontWeight={400}
                  color="#59AA7A"
                  _hover={{
                    bgColor: "#59AA7A",
                    color: "white",
                  }}
                  borderRadius="33px"
                  border="2px solid #59AA7A"
                  p="19px 31px"
                >
                  Selengkapnya
                </Button>
              </Link>
            </Box>
          </Center>
          {/* <Text
            fontSize="24px"
            textAlign="center"
            mt="13px"
            fontWeight={700}
            textDecoration="underline"
          >
            <Link as={NextLink} href="https://vocasia.id/">
              Selengkapnya
            </Link>
          </Text> */}
        </Box>
      </Box>
    </div>

    // testing
  );
}
