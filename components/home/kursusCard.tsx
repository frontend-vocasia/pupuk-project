import { Flex, Text, Link, chakra, Button } from "@chakra-ui/react";
import React from "react";
import Image from "next/image";
import NextLink from "next/link";
import CardImg from "../../public/images/img-card.svg";
import ImgKursus from "../../public/images/img-kursus-new.svg";
import ImgKursusBaru from "../../public/images/img-kursus-baru.svg";
import ImgKursusBaruLagi from "../../public/images/img-kursus-baru-lagi.svg";
import ImgKursusFix from "../../public/images/img-testi-fix.png";
import ImgInfo from "../../public/images/img-icon-info.svg";
import ImgHot from "../../public/images/img-icon-hot.svg";

import { AiOutlineRight } from "react-icons/ai";

type Props = {};

export default function KursusCard({
  title,
  description,
  to,
}: {
  title: string;
  description: any;
  to: string;
}) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) =>
      ["width", "height", "src", "alt"].includes(prop),
  });

  return (
    <Flex
      id="card"
      // maxW={{ base: "300px", lg: "280px", xl: "360px" }}
      h="auto"
      overflow="hidden"
      borderRadius="16px"
      direction="column"
      bgColor="#FFF"
      boxShadow="md"
      _hover={{ boxShadow: "xl" }}
      filter="drop-shadow(4px 10px 4px rgba(0, 0, 0, 0.2))"
    >
      <Flex id="cardImgCover" position="relative" direction="column">
        <Image
          src={ImgKursusFix}
          alt="image cover kursus"
          height={500}
          priority
        />
        <Flex position="absolute" right={0} top={0} zIndex={20}>
          <Image src={ImgInfo} alt={title} />
        </Flex>
      </Flex>
      <Flex id="cardContent" px={4} direction="column" flex={1} w="full">
        <Flex justify="space-between" align="flex-start" w="full" my={4}>
          <Flex w="90%">
            <Text
              fontSize="2xl"
              fontWeight="bold"
              noOfLines={2}
              color="#081F32"
            >
              {title}
            </Text>
          </Flex>
          <Flex w="10%" mt={1}>
            <Image src={ImgHot} alt={title} />
          </Flex>
        </Flex>

        <Flex fontSize="md" fontWeight="normal" px={4} color="#6E798C">
          {description}
        </Flex>
      </Flex>
      <Flex id="cardAction" p={4} w="full">
        <Link w="full" href={to} textDecoration="none">
          <Button
            w="full"
            bgColor="#2ECC71"
            borderRadius={8}
            color="white"
            _hover={{ bgColor: "#01352C" }}
          >
            Selengkapnya
          </Button>
        </Link>
      </Flex>
    </Flex>
  );
}
