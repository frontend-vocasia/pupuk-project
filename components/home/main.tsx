import Image from "next/image";
import { Box, chakra } from "@chakra-ui/react";
import ImgMain from "../../public/heroMain.png";
import ImgMainHome from "../../public/images/img-main-home.svg";

type Props = {};

export default function Main({}: Props) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) =>
      ["width", "height", "src", "alt", "priority"].includes(prop),
  });
  return (
    <Box position="relative" overflow="hidden">
      <OurImage
        src={ImgMainHome}
        priority
        alt="Dan Abramov"
        width="100%"
        objectFit="cover"
        borderRadius="0 0 0 150px"
        height="538px"
      />
    </Box>
  );
}
