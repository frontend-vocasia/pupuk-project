import React from "react";
import { Box, Image, Text, chakra } from "@chakra-ui/react";

type Props = {};

export default function TestiCard({
  image,
  text,
  name,
}: {
  image: any;
  text: string;
  name: string;
}) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) =>
      ["width", "height", "src", "alt"].includes(prop),
  });
  return (
    <div>
      <Box
        p="22px"
        borderRadius="25px"
        boxShadow="4px 10px 4px rgba(0, 0, 0, 0.2)"
        bgColor="rgba(255, 255, 255, 0.9);"
        overflow="hidden"
        w={{ xl: "90%", base: "95%", md: "80%", lg: "80%" }}
        h={{ xl: "100%", base: "100%", lg: "100%", md: "100%", sm: "650px" }}
        display="flex"
        flexDir={{ xl: "row", base: "column" }}
        alignItems="center"
        mx="auto"
        mt="50px"
        mb="50px"
      >
        <Box
          minW={{ xl: "400px", base: "150px", md: "30%" }}
          minH={{ xl: "400px", base: "150px", md: "30%" }}
        >
          <OurImage
            src={image.src}
            alt={image.alt}
            width={"100%"}
            height="100%"
            rounded="full"
          />
        </Box>
        <Box>
          <Text
            fontSize={{
              xl: "20px",
              base: "14px",
              lg: "18px",
              sm: "16px",
              md: "18px",
            }}
            textAlign={{ base: "justify", xl: "left" }}
            mb={4}
            mt={{ base: "20px", xl: "0", lg: "20px", md: "20px" }}
            ml={{ base: "0", md: "30px" }}
            color="#01352C"
          >
            {text}
          </Text>
          <Text
            fontSize={{
              xl: "32px",
              base: "22px",
              md: "26px",
              sm: "24px",
              lg: "30px",
            }}
            mb={4}
            // mt="40px"
            ml={{ xl:"30px", base: '0px' }}
            color="#01352C"
            fontWeight={700}
            display="flex"
            my="auto"
          >
            {name}
          </Text>
        </Box>
      </Box>
      {/* <Box zIndex={3} mt="30px" position="relative">
        <Image
          src={image.src}
          alt={image.alt}
          w={"120px"}
          h={"120px"}
          rounded="full"
          mx="auto"
        />
      </Box>
      <Box
        p="22px"
        borderRadius="15px"
        boxShadow="md"
        bgColor="#83D1A2"
        overflow="hidden"
        w={{ xl: "330px", base: "80%", md: "100%", lg: "330px" }}
        h="100%"
        mx="auto"
        mt="-40px"
      >
        <Box>
          <Text fontSize={{ xl: "20px", base: "18px" }} mb={4} mt="40px">
            {text}
          </Text>
        </Box>
      </Box> */}
    </div>
  );
}
