import { Box, Flex, Text, Image, Link, Grid, GridItem } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import type { HTMLChakraProps } from '@chakra-ui/react';

interface MapProps extends HTMLChakraProps<'iframe'> {
  src: string;
  title: string;
}

const Map: React.FC<MapProps> = ({ src, title, ...rest }) => {
  return (
    <Box position="relative" overflow="hidden" {...rest}>
      <iframe
        title={title}
        src={src}
        width="100%"
        height="100%"
        frameBorder="0"
        style={{ top: 0, left: 0 }}
      />
    </Box>
  );
};

const Contact = () => {
  const phoneNumber = "031 8283976";
  const email = "pupuk.surabaya@pupuk.or.id";
  const alamat = "https://www.google.com/maps/place/Jl.+Karah+Tama+No.7,+Karah,+Kec.+Jambangan,+Kota+SBY,+Jawa+Timur+60232/@-7.3120504,112.7189135,17z/data=!3m1!4b1!4m6!3m5!1s0x2dd7fb7ef279fc53:0xbcabdc364d218a96!8m2!3d-7.3120504!4d112.7189135!16s%2Fg%2F11c4dfrgj4"
  const web = 'https://www.pupuk.or.id/'

  return(
    <Box 
      mt={{base: '40px', md: '90px'}}  
      mb='90px'
      px={{base: '20px', md: '70px'}} 
      py={{base: '30px', md: '52px'}} 
      zIndex='1' 
      color='black'
      width="1300px"
      height= {{base: '1150px', md: "1263px"}} 
      borderRadius="20px"
      backgroundColor="rgba(255, 255, 255, 0.95)"
      shadow="0px 10px 10px rgba(1, 53, 44, 0.25)"
    >
      <Flex direction='column' ml={{base: '0px', xl: '70px'}}>
        <Box >
          <Grid
            templateColumns={{ lg: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
            gap={5}
          >
            <GridItem>
            <Flex alignItems='start'>
              <Image src="./icon/icon-map.svg" alt="" mr={{base: "15px", md: "25px"}} w={{base: "40px", md: '60px', xl: "70px"}}/>
              <Flex direction='column' mt='10px'>
                <Text fontSize={{base: '20px', md: '25px', xl: '32px'}} fontWeight="bold" mb='20px'>Alamat Kantor</Text>
                <Link
                fontSize={{base: '14px', md: '18px', xl: '24px'}} 
                href={alamat}
                _hover={{ color: "#2ECC71" }}
                >
                  Jl. Karah Tama No. 7, Jambangan,<br></br> Surabaya, Jawa Timur 60232
                </Link>
              </Flex>
            </Flex>
            </GridItem>
            <GridItem>
              <Flex alignItems='start'>
                <Image src="./icon/icon-telp.svg" alt="" mr={{base: "15px", md: "25px"}} w={{base: "40px", md: '60px', xl: "70px"}}/>
                <Flex direction='column' mt='10px'>
                  <Text fontSize={{base: '20px', md: '25px', xl: '32px'}} fontWeight="bold" mb='20px'>Telephone</Text>
                  <Link 
                    fontSize={{base: '14px', md: '18px', xl: '24px'}} 
                    href={`tel:${phoneNumber}`}
                    _hover={{ color: "#2ECC71" }}
                  >
                    031 8283976
                  </Link>
                </Flex>
              </Flex>
            </GridItem>
          </Grid>
        </Box>
        <Box mt={23}>
          <Grid
            templateColumns={{ lg: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
            gap={5}
          >
            <GridItem>
              <Flex alignItems='start'>
                <Image src="./icon/icon-web.svg" alt="" mr={{base: "15px", md: "25px"}} w={{base: "40px", md: '60px', xl: "70px"}}/>
                <Flex direction='column' mt='10px'>
                  <Text fontSize={{base: '20px', md: '25px', xl: '32px'}} fontWeight="bold" mb='20px'>Website</Text>
                  <Link 
                    fontSize={{base: '14px', md: '18px', xl: '24px'}} 
                    textDecoration='underline'
                    href={web}
                    _hover={{ color: "#2ECC71" }}
                  >
                    https://www.pupuk.or.id/
                  </Link>
                </Flex>
              </Flex>
            </GridItem>
            <GridItem>
              <Flex alignItems='start'>
                <Image src="./icon/icon-mail.svg" alt="" mr={{base: "15px", md: "25px"}} w={{base: "40px", md: '60px', xl: "70px"}}/>
                <Flex direction='column' mt='10px'>
                  <Text fontSize={{base: '20px', md: '25px', xl: '32px'}} fontWeight="bold" mb='20px'>Email</Text>
                  <Link 
                    fontSize={{base: '14px', md: '18px', xl: '24px'}} 
                    href={`mailto:${email}`}
                    _hover={{ color: "#2ECC71" }}
                  >
                    pupuk.surabaya@pupuk.or.id
                  </Link>
                </Flex>
              </Flex>
            </GridItem>
          </Grid>
        </Box>
      </Flex>

    <Map
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63324.61459956843!2d112.67538593914304!3d-7.264904057501923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fb7ebb1016d1%3A0x809e05cc954ea043!2zUOKAolXigKJQ4oCiVeKAoksgU3VyYWJheWE!5e0!3m2!1sid!2sid!4v1678431451392!5m2!1sid!2sid"
      title="Google Maps"
      h={{base: '550px', lg: '780px'}}
      mt='60px'
      borderRadius='10px'
    />

    </Box>
  ) 
}


export default Contact;