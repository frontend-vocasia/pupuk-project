import { create } from "zustand";

type PropsTitle = {
  title: string;
  updateTitle: (newtitle: string) => void;
};

export const useModalTitle = create<PropsTitle>((set, get) => ({
  title: "",
  updateTitle: (newTitle: string) => {
    set({ title: newTitle });
  },
}));

type PropsImg = {
  img: string;
  updateImg: (newImg: string) => void;
};

export const useModalImg = create<PropsImg>((set, get) => ({
  img: "",
  updateImg: (newImg: string) => {
    set({ img: newImg });
  },
}));
