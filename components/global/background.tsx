import Image from "next/image";
import patternSquare from "public/images/pattern-square.svg";
import patternCircle from "public/images/pattern-circle.svg";
import patternEllipse from "public/images/pattern-ellipse.svg";
import { Flex } from "@chakra-ui/react";

export default function Background() {
  return (
    <Flex
      w="full"
      h="full"
      overflow="hidden"
      position="absolute"
      left={0}
      zIndex={0}
    >
      <Flex position="absolute" left={0} mt="40vh">
        <Image src={patternSquare} alt="pattern background" />
      </Flex>
      <Flex position="absolute" right={0} bottom={0} overflow="hidden">
        <Image
          src={patternCircle}
          alt="pattern background"
          style={{ marginBottom: "-100px" }}
        />
      </Flex>
      <Flex position="absolute" left={0} bottom={0} overflow="hidden">
        <Image
          src={patternEllipse}
          alt="pattern background"
          style={{ marginBottom: "-100px" }}
        />
      </Flex>
    </Flex>
  );
}
