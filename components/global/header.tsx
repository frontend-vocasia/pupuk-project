import React from "react";
import { Flex, Text } from "@chakra-ui/react";

type Props = {
  title: string;
};

export default function Header({ title }: Props) {
  return (
    <Flex
      w="full"
      h="350px"
      bgColor="#01352C"
      background= "url(https://i.ibb.co/dctnYL4/bg-Header2.png)"
      backgroundSize="cover"
      borderBottomLeftRadius={{base: "100px", md: "150px"}}
      align="center"
      justify="center"
      zIndex={10}
    >
      <Text color="#FFF" fontSize={{ base: "30px", md: "64px" }} fontWeight="bold">
        {title}
      </Text>
    </Flex>
  );
}
