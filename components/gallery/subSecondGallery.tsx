import Image from "next/image";
import gallery from "@/styles/Gallery.module.css";
import { Box } from "@chakra-ui/react";
import { useModalImg, useModalTitle } from "../features/useModal";

interface propsImage {
  hFirst: string;
  hSecond: string;
  src: any;
  alt: string;
  onClick: () => void;
}

function SubSecondGallery({ hFirst, hSecond, src, alt, onClick }: propsImage) {
  const updateImg = useModalImg((state) => state.updateImg);
  const updateTitle = useModalTitle((state) => state.updateTitle);

  return (
    <Box
      w="93%"
      h={{ base: hFirst, md: hSecond }}
      borderRadius="15px"
      margin={{ base: "12px auto", md: "15px auto", lg: "27px auto" }}
      onClick={() => {
        updateImg(src);
        updateTitle(alt);
        onClick();
      }}
      overflow="hidden"
      className={gallery.boxImg1}
      cursor="pointer"
    >
      <Image src={src} alt={alt} style={{ width: "100%", height:"100%", objectFit: "cover" }} />
    </Box>
  );
}

export default SubSecondGallery;
