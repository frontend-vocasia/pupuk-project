import Image from "next/image";
import { useModalImg, useModalTitle } from "../features/useModal";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  Text,
} from "@chakra-ui/react";

interface propsModal {
  isOpen: any;
  onClose: any;
}

export default function ModalGallery({ isOpen, onClose }: propsModal) {
  const img = useModalImg((state) => state.img);
  const title = useModalTitle((state) => state.title);

  return (
    <>
      <div>
        <Modal
          blockScrollOnMount={false}
          isCentered
          isOpen={isOpen}
          onClose={onClose}
          size={{ base: "xs", md: "sm", lg: "xs" }}
        >
          <ModalOverlay bg="blackAlpha.800" backdropFilter="blur(5px)" />
          <ModalContent borderRadius="12px" boxShadow="none" bg="transparent" p="auto 0" m="auto 0">
            <ModalCloseButton color="white" />
            <Image
              src={img}
              alt="Dokumentasi Pertama"
              style={{ width: "100%", height: "auto", borderRadius: "15px" }}
            />
            <Text
              color="white"
              textAlign="center"
              fontSize={{ base: "lg", md: "2xl" }}
              mt={5}
            >
              {title}
            </Text>
          </ModalContent>
        </Modal>
      </div>
    </>
  );
}
