import React from "react";
import { Box, Image, Text, chakra } from "@chakra-ui/react";

type Props = {};

export default function ImageItem({
  image,
  text,
}: {
  image: any;
  text: string;
}) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) => ["w", "h", "src", "alt"].includes(prop),
  });
  return (
    <div>
      <OurImage
        src={image.src}
        alt={text}
        objectFit="cover"
        boxShadow="0px 4px 4px rgba(0, 0, 0, 0.25)"
        overflow="hidden"
        w="100%"
        h="auto"
        mx="auto"
      />
    </div>
  );
}
