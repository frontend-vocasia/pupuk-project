import { Box, Flex, useDisclosure, Text, Button } from "@chakra-ui/react";
import ModalGallery from "./modalGallery";
import TitleHeader from "../global/header";
import BgHeader from "../global/background";
import SwiperCore, { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import CarouselItem from "./carouselItem";
import carouselBg1 from "../../public/images/gallery/carouselBg.jpeg";
import carouselBg2 from "../../public/images/gallery/carouselBg2.jpeg";
import img1 from "@/public/images/gallery/1.png";
import img2 from "@/public/images/gallery/2.jpeg";
import img3 from "@/public/images/gallery/3.jpeg";
import img4 from "@/public/images/gallery/4.jpeg";
import img5 from "@/public/images/gallery/5.jpeg";
import img6 from "@/public/images/gallery/6.jpeg";
import img7 from "@/public/images/gallery/7.jpeg";
import img8 from "@/public/images/gallery/8.jpeg";
import img9 from "@/public/images/gallery/9.jpeg";
import img10 from "@/public/images/gallery/10.jpeg";
import img11 from "@/public/images/gallery/11.jpeg";
import img12 from "@/public/images/gallery/12.jpeg";
import img13 from "@/public/images/gallery/13.jpeg";
import img14 from "@/public/images/gallery/14.jpeg";
import img15 from "@/public/images/gallery/15.jpeg";
import img18 from "@/public/images/gallery/18.jpg";
import img19 from "@/public/images/gallery/19.jpeg";
import img20 from "@/public/images/gallery/20.jpeg";
import img21 from "@/public/images/gallery/21.jpeg";
import img22 from "@/public/images/gallery/22.jpeg";
import img23 from "@/public/images/gallery/23.jpeg";
import img24 from "@/public/images/gallery/24.jpeg";
import img25 from "@/public/images/gallery/25.jpeg";
import img26 from "@/public/images/gallery/26.jpeg";
import img27 from "@/public/images/gallery/27.jpeg";
import img28 from "@/public/images/gallery/28.jpeg";
import img29 from "@/public/images/gallery/29.jpeg";
import img30 from "@/public/images/gallery/30.jpeg";
import ImageItem from "./imageItem";
import IframeYt from "./iframeYt";
import { useState } from "react";

export default function Gallery(): JSX.Element {
  const modalAdd = useDisclosure();
  SwiperCore.use([Navigation, Pagination]);
  const [btnAll, setBtnAll] = useState<boolean>(false);
  const firstImg = imageItem.slice(0, total());
  function total() {
    if (!btnAll) {
      return 9;
    } else {
      return 30;
    }
  }

  return (
    <Flex
      w="100%"
      minH="100vh"
      bgColor="#ECFFF4"
      direction="column"
      position="relative"
    >
      <BgHeader />
      <ModalGallery isOpen={modalAdd.isOpen} onClose={modalAdd.onClose} />
      {/* Header */}
      <TitleHeader title="Galeri" />
      <Box height={{ base: "50px", md: "53px" }}></Box>
      {/* slider */}
      <Flex mx="auto" w={{ base: "95%", md: "85%" }}>
        <Swiper
          slidesPerView={1}
          spaceBetween={0}
          centeredSlides={true}
          pagination={{ clickable: true }}
          navigation
        >
          {cardItem.map((item) => (
            <SwiperSlide key={item.id}>
              <CarouselItem image={item.image} text={item.text} />
            </SwiperSlide>
          ))}
        </Swiper>
      </Flex>
      <Box height={{ base: "50px", md: "53px" }}></Box>
      {/* FOTO KEGIATAN */}
      <Text
        textAlign="center"
        fontSize="3xl"
        fontWeight={600}
        color="rgba(1, 53, 44, 1)"
        mb="10px"
      >
        Foto Kegiatan
      </Text>
      <Flex
        zIndex={99}
        mx="auto"
        w={{ base: "98%", md: "85%" }}
        flexWrap="wrap"
      >
        {firstImg.map((item) => (
          <Box
            key={item.id}
            flex={{ base: "100%", sm: "40%", xl: "30%" }}
            height={{
              base: "230px",
              sm: "175px",
              md: "230px",
              xl: "260px",
            }}
            overflow="hidden"
            m="10px"
            borderRadius={{ base: "12px", md: "15px" }}
          >
            <ImageItem image={item.image} text={item.text} />
          </Box>
        ))}
      </Flex>
      <Button
        mx="auto"
        mt={{ base: "10px", md: "20px" }}
        variant="solid"
        bgColor="transparent"
        fontSize={{ base: "18px", xl: "20px" }}
        fontWeight={400}
        color="#59AA7A"
        _hover={{
          bgColor: "#59AA7A",
          color: "white",
        }}
        borderRadius="33px"
        border="2px solid #59AA7A"
        p="19px 31px"
        onClick={() => setBtnAll(!btnAll)}
      >
        {btnAll ? "Sembunyikan" : "Selengkapnya"}
      </Button>
      <Box height={{ base: "80px", md: "53px" }}></Box>
      {/* VIDEO */}
      <Text
        textAlign="center"
        fontSize="3xl"
        fontWeight={600}
        color="rgba(1, 53, 44, 1)"
        mb="10px"
        mx="1rem"
      >
        Video Pelatihan dan Cerita Sukses
      </Text>
      <Flex
        zIndex={99}
        mx="auto"
        w={{ base: "98%", md: "85%" }}
        flexWrap="wrap"
      >
        {linkYt.map((item) => (
          <Box
            key={item.id}
            flex={{ base: "100%", lg: "40%" }}
            m={{ base: 3, lg: 5 }}
            height={{ base: "180px", md: "330px" }}
          >
            <IframeYt link={item.link} />
          </Box>
        ))}
      </Flex>
      <Box height={{ base: "80px", md: "53px" }}></Box>
    </Flex>
  );
}

const cardItem: { id: number; image: any; text: string }[] = [
  {
    id: 1,
    image: carouselBg1,
    text: "Dokumentasi 1",
  },
  {
    id: 2,
    image: carouselBg2,
    text: "Dokumentasi 2",
  },
];

const imageItem: { id: number; image: any; text: string }[] = [
  {
    id: 1,
    image: img1,
    text: "Dokumentasi 1",
  },
  {
    id: 2,
    image: img2,
    text: "Dokumentasi 2",
  },
  {
    id: 3,
    image: img3,
    text: "Dokumentasi 3",
  },
  {
    id: 4,
    image: img4,
    text: "Dokumentasi 4",
  },
  {
    id: 5,
    image: img5,
    text: "Dokumentasi 5",
  },
  {
    id: 6,
    image: img6,
    text: "Dokumentasi 6",
  },
  {
    id: 7,
    image: img7,
    text: "Dokumentasi 7",
  },
  {
    id: 8,
    image: img8,
    text: "Dokumentasi 8",
  },
  {
    id: 9,
    image: img9,
    text: "Dokumentasi 9",
  },
  {
    id: 10,
    image: img10,
    text: "Dokumentasi 10",
  },
  {
    id: 11,
    image: img11,
    text: "Dokumentasi 11",
  },
  {
    id: 12,
    image: img12,
    text: "Dokumentasi 12",
  },
  {
    id: 13,
    image: img13,
    text: "Dokumentasi 13",
  },
  {
    id: 14,
    image: img14,
    text: "Dokumentasi 14",
  },
  {
    id: 15,
    image: img15,
    text: "Dokumentasi 15",
  },
  {
    id: 18,
    image: img18,
    text: "Dokumentasi 18",
  },
  {
    id: 19,
    image: img19,
    text: "Dokumentasi 19",
  },
  {
    id: 20,
    image: img20,
    text: "Dokumentasi 20",
  },
  {
    id: 21,
    image: img21,
    text: "Dokumentasi 21",
  },
  {
    id: 22,
    image: img22,
    text: "Dokumentasi 22",
  },
  {
    id: 23,
    image: img23,
    text: "Dokumentasi 23",
  },
  {
    id: 24,
    image: img24,
    text: "Dokumentasi 24",
  },
  {
    id: 25,
    image: img25,
    text: "Dokumentasi 25",
  },
  {
    id: 26,
    image: img26,
    text: "Dokumentasi 26",
  },
  {
    id: 27,
    image: img27,
    text: "Dokumentasi 27",
  },
  {
    id: 28,
    image: img28,
    text: "Dokumentasi 28",
  },
  {
    id: 29,
    image: img29,
    text: "Dokumentasi 29",
  },
  {
    id: 30,
    image: img30,
    text: "Dokumentasi 30",
  },
];

const linkYt = [
  {
    id: 1,
    link: "https://www.youtube.com/embed/fKO12jzrkIs",
  },
  {
    id: 2,
    link: "https://www.youtube.com/embed/JJBNtoDtrSM",
  },
  {
    id: 3,
    link: "https://www.youtube.com/embed/zup3IrqmenM",
  },
  {
    id: 4,
    link: "https://www.youtube.com/embed/duQ64VDvP3c",
  },
  {
    id: 5,
    link: "https://www.youtube.com/embed/X_UpwRM0dbE",
  },
  {
    id: 6,
    link: "https://www.youtube.com/embed/LLpvp7fPfrA",
  },
  {
    id: 7,
    link: "https://www.youtube.com/embed/F1rR5y58zi0",
  },
  {
    id: 8,
    link: "https://www.youtube.com/embed/_0vbZdy7dOE",
  },
];
