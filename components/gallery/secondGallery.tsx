import { Box } from "@chakra-ui/react";

function SecondGallery({ children }: any) {
  return (
    <Box zIndex={1} flex={{ base: "100%", md: "30%" }}>
      {children}
    </Box>
  );
}

export default SecondGallery;
