import { Box } from "@chakra-ui/react";
import Image from "next/image";
import gallery from "@/styles/Gallery.module.css";
import { useModalImg, useModalTitle } from "../features/useModal";

interface onClick {
  onClick: () => void;
  title: string;
  imgGallery: any;
}

export default function FirstGallery({ onClick, title, imgGallery }: onClick) {
  const updateImg = useModalImg((state) => state.updateImg);
  const updateTitle = useModalTitle((state) => state.updateTitle);

  return (
    <Box
      w={{ base: "93%", md: "350px", lg: "366px" }}
      overflow="hidden"
      h="312px"
      borderRadius="15px"
      margin={{ base: "8px auto", md: "8px 8px", lg: "32px" }}
      zIndex={1}
      className={gallery.boxImg1}
      onClick={() => {
        updateImg(imgGallery);
        updateTitle(title);
        onClick();
      }}
    >
      <Image priority src={imgGallery} alt={title} style={{ width: "100%", height:"100%", objectFit: "cover" }} />
    </Box>
  );
}
