import React from "react";
import { Box, Image, Text, chakra } from "@chakra-ui/react";

type Props = {};

export default function CarouselItem({
  image,
  text,
}: {
  image: any;
  text: string;
}) {
  const OurImage = chakra(Image, {
    shouldForwardProp: (prop) => ["w", "h", "src", "alt"].includes(prop),
  });
  return (
    <div>
      <OurImage
        src={image.src}
        alt={text}
        objectFit="cover"
        borderRadius={{ base: "12px", md: "25px" }}
        boxShadow="0px 4px 4px rgba(0, 0, 0, 0.25)"
        overflow="hidden"
        w="100%"
        h={{ xl: "500px", base: "260px", lg: "100%" }}
        mx="auto"
      />
    </div>
  );
}
