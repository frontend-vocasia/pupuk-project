interface props {
  link: string;
}
export default function IframeYt({ link }: props) {
  return (
    <iframe
      width="100%"
      height="100%"
      src={link}
      title="YouTube video player"
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      style={{
        borderRadius: "15px",
      }}
    ></iframe>
  );
}
